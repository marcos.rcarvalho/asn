asn (0.78.3-1) unstable; urgency=medium

  * New upstream version "0.78.3".

  [ oday, Christoph Biedl]
  * debian/control: added dependency option for mtr-tiny or mtr.

 -- Marcos Rodrigues de Carvalho (aka oday) <marcosrcarvalho42@gmail.com>  Fri, 07 Feb 2025 17:50:05 -0300

asn (0.78.0-1) unstable; urgency=medium

  * New upstream version "0.78.0".

 -- Marcos Rodrigues de Carvalho (aka oday) <marcosrcarvalho42@gmail.com>  Thu, 03 Oct 2024 20:37:13 -0300

asn (0.77.0-1) unstable; urgency=medium

  * New upstream version "0.77.0".
  * Remove debian/manpage/asn.1 because the upstream
    is providing one manual page.
  * debian/manpages file pointing to upstream.
  * Run wrap-and-sort -a.
  * debian/tests/run-unit-test define the pkg variable to
    avoid shellcheck SC2154 error.
  * debian/watch by removing unnecessary line breaks.

 -- Marcos Rodrigues de Carvalho (aka oday) <marcosrcarvalho42@gmail.com>  Tue, 16 Jul 2024 15:15:02 -0300

asn (0.76.1-2) unstable; urgency=medium

  * debian/control: Bump Standards-Version to "4.7.0".

 -- Marcos Rodrigues de Carvalho (aka oday) <marcosrcarvalho42@gmail.com>  Tue, 30 Apr 2024 14:13:19 -0300

asn (0.76.1-1) unstable; urgency=medium

  * New upstream version "0.76.1".
  * debian/manpage: Update manpage.
  * debian/gbp.conf: Added the gbp configuration file.

 -- Marcos Rodrigues de Carvalho (aka oday) <marcosrcarvalho42@gmail.com>  Wed, 28 Feb 2024 17:56:51 -0300

asn (0.75.3-1.1) unstable; urgency=medium

  * Non-maintainer Upload.
    + Source-only Upload for testing migration.

 -- Nilesh Patra <nilesh@debian.org>  Wed, 21 Feb 2024 00:49:08 +0530

asn (0.75.3-1) unstable; urgency=medium

  [ Nilesh Patra, oday ]
  * Initial release. (Closes: #1064033)

 -- Marcos Rodrigues de Carvalho (aka oday) <marcosrcarvalho42@gmail.com>  Sat, 17 Feb 2024 16:54:44 +0530
